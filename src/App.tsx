import React from 'react';
import Chat from './components/Chat/Chat';

const App: React.FC = () => (
  <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
);

export default App;
