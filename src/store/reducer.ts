import { combineReducers } from 'redux';

import chat from '../state/chat/chat.reducer';

const rootReducer = combineReducers({
  chat,
});

export type IRootState = ReturnType<typeof rootReducer>;

export default rootReducer;
