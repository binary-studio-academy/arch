import { IChatState, IChatAction } from './chat.interfaces';
import { 
  SET_MESSAGES, 
  ADD_MESSAGE, 
  SET_PRELOADER, 
  EDIT_MESSAGE, 
  SET_EDIT_MESSAGE_ID, 
  SET_EDIT_MODE, 
  DELETE_MESSAGE 
} from './chat.types';

const initialState:IChatState = {
  messages: [],
  editModal: false,
  preloader: true,
  activeUserId: '5328dba1-1b8f-11e8-9629-c7eca82aa7bd',
  editMessageId: '',
  chatName: 'Binary',
};

export default (state=initialState, action:IChatAction):IChatState => {
  switch (action.type) {
  case EDIT_MESSAGE:
  case SET_MESSAGES:
    return { ...state, messages: action.payload };
  case ADD_MESSAGE:
    return { ...state, messages: [...state.messages, action.payload] };
  case SET_PRELOADER:
    return { ...state, preloader: action.payload };
  case SET_EDIT_MODE:
    return { ...state, editModal: action.payload };
  case SET_EDIT_MESSAGE_ID:
    return { ...state, editMessageId: action.payload };
  case DELETE_MESSAGE:
    return { ...state, messages: state.messages.filter(message => message.id !== action.payload) };
  default:
    return state;
  }
};