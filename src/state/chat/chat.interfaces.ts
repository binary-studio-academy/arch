import { IMessage } from '../../interfaces';

export interface IChatState {
  messages: IMessage[],
  editModal: boolean,
  preloader: boolean,
  activeUserId: string,
  editMessageId: string,
  chatName: string,
}

export interface IChatAction {
  type: string,
  payload?: any, // eslint-disable-line
}

export interface ISetMessages {
  type: string,
  payload: IMessage[]
}

export interface IAddMessage {
  type: string,
  payload: IMessage
}

export interface IDeleteMessage {
  type: string,
  payload: string
}

export interface IPreloaderAction {
  type: string,
  payload: boolean,
}

export interface IEditMessage {
  type: string,
  payload: IMessage[],
}

export interface IEditMode {
  type: string,
  payload: boolean,
}

export interface IEditMessageId {
  type: string,
  payload: string,
}