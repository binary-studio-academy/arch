import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import styles from './message.module.css';
import likeIcon from './assets/like.svg';
import optionIcon from './assets/more-options.svg';

import { IMessageContainer, IMessage } from '../../../../interfaces';
import { formattedTime } from '../../../../helpers/chat';
import { IRootState } from '../../../../store/reducer';
import { setEditMode, setEditMessageId, setEditMessage, deleteMessage } from '../../../../state/chat/chat.actions';

const Message: React.FC<IMessageContainer> = ({ message }) => {
  const dispatch = useDispatch();
  
  const { messages, activeUserId } = useSelector((state:IRootState) => ({
    messages: state.chat.messages,
    activeUserId: state.chat.activeUserId,
  }));

  const [optionsActive, setOptionsActive] = useState(false);

  const isUserMessage = activeUserId === message.userId;

  const handleChange = () => {
    dispatch(setEditMessageId(message.id));
    dispatch(setEditMode(true));
    setOptionsActive(false);
  };

  const handleLike = ():void => {
    const updatedMessages:IMessage[] = [ ...messages.map(messageData => {
      if (messageData.id === message.id) {
        if (!messageData.likes.includes(activeUserId)) {
          return { ...messageData, likes: [...messageData.likes, activeUserId] };
        }
        
        return { ...messageData, likes: [...messageData.likes.filter(id => id !== activeUserId)] };
      }

      return messageData;
    })] || [];

    dispatch(setEditMessage(updatedMessages));
  };
  
  const handleDelete = () => {
    dispatch(deleteMessage(message.id));
  };

  return (
    <li className={isUserMessage ? `${styles.ownMessage} own-message` : `${styles.message} message`}>
      {!isUserMessage && <img className={`${styles.avatar} message-user-avatar`} src={message.avatar} alt={`Аватар пользователя ${message.user}`} /> }
      <div className={styles.messageWrapper}>
        <div className={styles.messageBody}>
          <p className={`${styles.messageText} message-text`}>
            {message.text}
          </p>
          <span className={`${styles.messageTime} message-time`}>{formattedTime(message.createdAt)}</span>
          {!isUserMessage && (
            <button className={`${styles.likes} ${message.likes.length ? 'message-liked' : 'message-like'}`} type="button" onClick={handleLike}>
              <span className={styles.likesCount}>{message.likes.length}</span>
              <img src={likeIcon} alt="понравилось" />
              <span className="visually-hidden">Лайк</span>
            </button>
          )}
        </div>
        {!isUserMessage && <h5 className={`${styles.userName} message-user-name`}>{message.user}</h5> }
      </div>
      {isUserMessage && (
        <>
          <button type="button" className={styles.optionButton} onClick={() => setOptionsActive(!optionsActive)}>
            <img src={optionIcon} alt="Опции" />
            <span className="visually-hidden">options</span>
          </button>
          <ul className={optionsActive ? styles.options : styles.optionDisabled}>
            <li className={styles.option}>
              <button className={`${styles.messageEdit} message-edit`} type="button" onClick={handleChange}>
                Изменить
              </button>
            </li>
            <li className={styles.option}>
              <button className={`${styles.messageDelete} message-delete`} type="button" onClick={handleDelete}>
                Удалить
              </button>
            </li>
          </ul>
        </>
      )}
    </li>
  );
};

export default Message;