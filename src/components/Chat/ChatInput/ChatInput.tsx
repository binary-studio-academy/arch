import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import style from './chatInput.module.css';

import { getUserFromMessages, uniqueId } from '../../../helpers/chat';
import { addMessage, setEditMode, setEditMessageId } from '../../../state/chat/chat.actions';
import { IRootState } from '../../../store/reducer';
import { IMessage } from '../../../interfaces';

const ChatInput: React.FC = () => {
  const dispatch = useDispatch();

  const { messages, activeUserId } = useSelector((state: IRootState) => ({
    messages: state.chat.messages, 
    activeUserId: state.chat.activeUserId,
  }));

  const [text, setText] = useState('');

  const handleSendMessage = ():void => {
    if (text) {
      const userData = getUserFromMessages(messages, activeUserId);
      const message:IMessage = {
        id: uniqueId(),
        userId: activeUserId || '',
        avatar: userData?.avatar || '',
        user: userData?.name || '',
        text,
        createdAt: new Date().toISOString(),
        editedAt: '',
        likes: [],
      };

      setText('');
      dispatch(addMessage(message));
    }
  };

  const handleonKeyDown = (ev:React.KeyboardEvent):void => {
    if (ev.code === 'ArrowUp') {
      const userMessages = messages.filter((message) => message.userId === activeUserId);
      const lastUserMessage = userMessages[userMessages.length - 1];
      
      if (lastUserMessage) {
        dispatch(setEditMessageId(lastUserMessage.id));
        dispatch(setEditMode(true));
      }
    }
  };

  return (
    <div className={`${style.inputWrapper} message-input`}>
      <textarea 
        className={`${style.textArea} message-input-text`}
        value={text} 
        onChange={(ev: React.FormEvent<HTMLTextAreaElement>) => setText(ev.currentTarget.value)}
        onKeyDown={handleonKeyDown}
      />
      <button className={`${style.submitButton} message-input-button`} type="button" onClick={handleSendMessage}>Отправить</button>
    </div>
  );
};

export default ChatInput;
