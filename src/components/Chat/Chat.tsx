/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styles from './chat.module.css';
import ChatHeader from './ChatHeader/ChatHeader';
import ChatBody from './ChatBody/ChatBody';
import ChatInput from './ChatInput/ChatInput';

import { errorHandler, getUserFromMessages, getUsersCount } from '../../helpers/chat';
import { IChatContainerProps, IMessage, IMessageResponse, IUser } from '../../interfaces';
import { setMessages, setPreloader } from '../../state/chat/chat.actions';
import { IRootState } from '../../store/reducer';

const Chat:React.FC<IChatContainerProps> = ({ url }) => {
  const dispatch = useDispatch();

  const { messages, activeUserId } = useSelector((state:IRootState) => ({
    messages: state.chat.messages,
    activeUserId: state.chat.activeUserId,
  }));

  useEffect(() => {
    dispatch(setPreloader(true));
    
    fetch(url)
      .then(async (res:Response) => {
        if (res.ok && res) {
          const json = await res.json();
          const updatedMessages:IMessage[] = json.map((messageData:IMessageResponse) => ({ ...messageData, likes: []})); 

          dispatch(setMessages(updatedMessages));
        } else {
          console.warn('Message request error.');
        }

        dispatch(setPreloader(false));
      })
      .catch(err => {
        dispatch(setPreloader(false));
        errorHandler(err);
      });
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const user: IUser = getUserFromMessages(messages, activeUserId || '');
  const usersCount:number = getUsersCount(messages);
  const lastMessageTime = messages.length && messages[messages.length - 1].createdAt || '';
   
  return (
    <div className={`${styles.chatWrapper} chat`}>
      <ChatHeader 
        user={user} 
        userCount={usersCount} 
        messagesCount={messages.length} 
        lastMessageTime={lastMessageTime} 
      />
      <ChatBody />
      <ChatInput />
    </div>
  );
};

export default Chat;