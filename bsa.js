import Chat from './src/components/Chat/Chat'; 
import rootReducer from './src/store/reducer';

export default {
  Chat,
  rootReducer,
};